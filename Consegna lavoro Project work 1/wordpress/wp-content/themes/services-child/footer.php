</div>
    <footer id="footer">
        <?php if ( is_active_sidebar( 'footer-widget-area' ) ) : ?>
        <aside id="footer-sidebar" role="complementary">
            <div id="fsidebar" class="widget-area">
                <ul class="xoxo">
                    <?php dynamic_sidebar( 'footer-widget-area' ); ?>
                </ul>
            <div class="clear"></div>
            </div>
        </aside>
        <?php endif; ?>
        <?php if ( has_nav_menu( 'footer-menu' ) ) : ?>
        <div id="fmenu"><?php wp_nav_menu( array( 'theme_location' => 'footer-menu', 'depth' => '1' ) ); ?>
        </div>
            <?php endif; ?>
            <div class="container footer-container">
                <div class="row align-items-end">
                        <div class="col-5">
                            <img src="<?php echo home_url(); ?>/wp-content/uploads/2019/03/logo_project_work.jpg">
                        </div>
                        <div class="col-2">
                        </div>
                        <div class="col-5">
                                <h4 class="footer-h4">Contatti:</h4>
                                    <ul>
                                        <li>Telefono: 0541 367100</li>
                                        <li>Fax: 0541 784001</li>
                                        <li>Email: info@enaiprimini.org</li>
                                    </ul>
                        </div>
                </div>
                <div class="footer-cite">
                    <cite>&#169;Credits Paola Bianchini 2019</cite>
                </div>
            </div> 
    </footer>
    </div>
    <?php wp_footer(); ?>
</body>
</html>

