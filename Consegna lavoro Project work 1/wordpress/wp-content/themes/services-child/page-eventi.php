<?php get_header(); ?>
<main id="content">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="entry-content">
        <div class="container">
            <div class="row">
                <div class="col-8">
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                        <header class="header">
                            <h1 class="entry-title"><?php the_title(); ?></h1>
                        </header>
                        
                            <!--****** richiamo la funzione eventi loop page_eventi per visualizzare l'elenco degli eventi in programma ******-->
                            <?php  eventi_loop_page_eventi(); ?>
                            <?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
                            <?php the_content(); ?>
                            <div class="entry-links"><?php wp_link_pages(); ?></div>
                    </article>
                </div>

                <?php if ( ! post_password_required() ) comments_template( '', true ); ?>
                <?php endwhile; endif; ?> 
                <div class="col-4 ricerca-evento">
                        <h3 >Ricerca Evento</h3>
                        <div id="search-eventi" clasentry-titles="ricerca-eventi">
                            <?php get_search_form(); ?>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <script>
       <?php script_eventi(); ?>;
    </script> 
     <?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>  
</main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
