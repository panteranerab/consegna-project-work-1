<?php get_header(); ?>
    <main id="content">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="container">
                <div class="row">
                    <div class="col-9">
                        <header class="header">
                            <h1 class="entry-title"><?php the_title(); ?></h1>
                        </header>
                    </div>
            
                    <div class="col-8">
                        <div class="entry-content">
                            <?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
                            <?php the_content(); ?> 
                            <div class="entry-links"><?php wp_link_pages(); ?></div>
                        </div>   
                    </div>
                    <div class="col-4 eventi-home" >
                        <header class="header">
                                <h3 class="entry-title">Prossimi Eventi</h3>
                        </header>
                        <!--******** richiamo la funzione per la visualizzazione degli ultimi 4 eventi in programma *******-->
                        <?php eventi_loop_page_home(); ?>
                        <!-- immagine cartolina di rimini -->
                        <br>
                        <div class="img_eventi-home_page"> <img src='<?php echo home_url(); ?>/wp-content/uploads/2019/03/rimini-italija.jpg'></div>
                    </div>
                </div>
            </div>
        </article>

        <?php if ( ! post_password_required() ) comments_template( '', true ); ?>
        <?php endwhile; endif; ?>
        <?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>
    </main>
<?php get_sidebar(); ?>
<?php get_footer(); ?>