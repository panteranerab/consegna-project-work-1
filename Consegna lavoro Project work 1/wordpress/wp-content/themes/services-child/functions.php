<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
 
}

       
//******* funzioni che convertono data e ora da formato db a formato utente********   

function conversione_data_ora_db($d){

    $data_sub= substr($d,0,-9);
    $data_conver=explode("-",$data_sub);
    $ora_sub=substr($d,-8,5);
    $ora_conver=explode(":",$ora_sub);
    return $data_conver[2]."/".$data_conver[1]."/".$data_conver[0]."  ".$ora_conver[0].":".$ora_conver[1];  
 }

function converti_data_ora_inizio($meta){
    $data_inizio_db=$meta['data_e_ora_inizio'][0];
    $data_inizio=conversione_data_ora_db($data_inizio_db);
    return $data_inizio;

}
function converti_data_ora_fine($meta){
    $data_fine_db=$meta['data_e_ora_fine'][0];
    $data_fine=conversione_data_ora_db($data_fine_db);
    return $data_fine;
}


//*******funzione che visualizza nella pagina eventi l'elenco dei custom post gestione eventi********
function eventi_loop_page_eventi() {
    $args = array(
            'post_type'  => 'gestione_eventi',
            'meta_key' => 'data_e_ora_inizio',
            'meta_type' => 'DATETIME',
            'orderby'=>'data_e_ora_inizio',
            'posts_per_page' => -1,
            'order' => 'DESC'
    );
   
    $eventi = new WP_Query($args);
       if($eventi->have_posts()) {   

        for($i=0; $i< $eventi->post_count; $i++){
            $meta = get_post_meta($eventi->posts[$i]->ID);

        //****** ricerco url relativo ad immagine inserita utilizzando id********
            $immagine=$meta['immagine'][0];
            $url_immagine=wp_get_attachment_image_src($immagine,'medium-large');         

       //****** creo una variabile per il post name e il post title*******
            $name=$eventi->posts[$i]->post_name; 
            $titolo=$eventi->posts[$i]->post_title;

       //****** impagino i dati  ********** 
            echo '<div class= "preview">
                    <button class="bottone-preview-evento">'.$titolo.'</button>
                        <div class="preview-evento">
                            <div class="card">
                                <img src='.$url_immagine[0].' class="card-img-top" alt="...">
                                <div class="card-body">
                                    <p class="card-text">
                                        <table class="table table-bordered">
                                            <thead class="thead-dark">
                                                <tr class="tabella-preview-eventi">
                                                    <th scope="col">Luogo Evento</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="tabella-preview-eventi">
                                                    <td>'.$meta['luogo'][0].'</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </p>
                                    <p class="card-text">
                                        <table class="table table-bordered">
                                            <thead class="thead-dark">
                                                <tr class="tabella-preview-eventi">
                                                    <th scope="col">Data e Ora inizio</th>
                                                    <th scope="col">Data e Ora fine</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr class="tabella-preview-eventi">
                                                    <td>'.converti_data_ora_inizio($meta).'</td>
                                                    <td>'.converti_data_ora_fine($meta).'</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </p>
                                    <a href="'.home_url().'/gestione_eventi/'.$name.'/" class="btn bottone-dettaglio-evento">Dettagli</a>
                                </div>
                            </div>
                        </div>
                 </div>';
        }  
     }
}

// *****funzione per script che mostra e nasconde div con preview evento *****

function script_eventi(){
            echo 'var coll = document.getElementsByClassName("bottone-preview-evento");
                    var i;
                    
                    for (i = 0; i < coll.length; i++) {
                        coll[i].addEventListener("click", function() {
                            this.classList.toggle("attivo");
                            var content = this.nextElementSibling;
                                if (content.style.display === "block") {
                                    content.style.display = "none";
                                } else {
                                    content.style.display = "block";
                                }
                        });
                    }';
}

//*******funzione che visualizza nella pagina home eventi l'elenco dei custom post gestione eventi********
function eventi_loop_page_home() {
    $args = array(
            'post_type'  => 'gestione_eventi',
            'meta_key' => 'data_e_ora_inizio',
            'meta_type' => 'DATETIME',
            'orderby'=>'data_e_ora_inizio',
            'posts_per_page' => 4,
            'order' => 'DESC'
    );
   
    $eventi = new WP_Query($args);
       if($eventi->have_posts()) {
        echo '<ul class="eventi_home_page">';
        for($i=0; $i< $eventi->post_count; $i++){

            $name=$eventi->posts[$i]->post_name; 
            $titolo=$eventi->posts[$i]->post_title;
            echo '<li><a href="'.home_url().'/gestione_eventi/'.$name.'/">'.$titolo.'</a></li>';      
        } 
        echo '</ul>';       
     }
}

// ****** funzione per single-gestione_eventi: ricerco url relativo ad immagine inserita utilizzando id ********

function immagine_evento($meta){
        $immagine=$meta['immagine'][0];
        $url_immagine=wp_get_attachment_image_src($immagine,'medium-large');
        return $url_immagine[0];
}


// ****** funzione per recuperare dati ed impaginazione single-gestione_eventi ******
function evento_impaginazione($meta){
                echo '
                    <div class="entry-content">
                       
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <header class="evento">
                                        <h3 >'. $meta['nome_evento'][0].'</h3>
                                    </header>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-12 evento"> 
                                                <h5><i class="fas fa-map-marker-alt"></i> Luogo</h5>
                                            </div>
                                            <div class="col-12 evento"> 
                                                <span>'. $meta['luogo'][0].'</span>
                                            </div>
                                            <div class="col-6 evento">
                                                <h5><i class="fas fa-clock"></i> Data e ora inizio</h5>
                                                <span>'.converti_data_ora_inizio($meta).'</span>
                                            </div>
                                            <div class="col-6 evento"> 
                                                <h5><i class="fas fa-clock"></i> Data e ora fine</h5>
                                                <span>'.converti_data_ora_fine($meta).'</span>
                                            </div>
                                            <div class="col-12 evento"> 
                                                 <h5><i class="fas fa-wheelchair"></i> Accesso Disabili</h5>
                                            </div>
                                            <div class="col-12 evento"> 
                                                <span>';
                                                        if($meta['accesso_per_disabili'][0]=="Si"){
                                                                echo 'Evento accessibile a persone con ridotta mobilità motoria';
                                                            }else{
                                                                echo 'Evento NON accessibile a persone con ridotta mobilità motoria';
                                                        };
                                        echo   '</span>
                                            </div>
                                            <div class="col-12 evento"> 
                                                <h5><i class="fas fa-ticket-alt"></i> Prezzo</h5>
                                            </div>
                                            <div class="col-12 evento"> 
                                                <span>';
                                                    if(($meta['prezzo_intero'][0]==0) && ($meta['prezzo_ridotto'][0]==0) &&($meta['prezzo_bambino'][0]==0)){
                                                            echo "Ingresso Libero";
                                                    }else{                    
                                                        if($meta['prezzo_intero'][0] !=0){ 
                                                            echo "Intero ".$meta['prezzo_intero'][0]." &euro;  " ;
                                                        }
                                                        if($meta['prezzo_ridotto'][0] !=0){ 
                                                            echo "Ridotto ".$meta['prezzo_ridotto'][0]." &euro;  " ;
                                                        }
                                                        if($meta['prezzo_bambino'][0] !=0){ 
                                                            echo "Bambino ".$meta['prezzo_bambino'][0]." &euro;" ;     
                                                        } 
                                                    };
                                        echo   '</span>
                                            </div>
                                            
                                        </div>
                            <div class="row">
                                    </div>
                                </div>
                            </div>
                                <div class="col-md-auto evento">
                                    <img src= '.immagine_evento($meta).'
                                </div>
                                <div class="col col-lg-2">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="col-6 evento-descrizione">
                                <h5>Descrizione</h5>
                                <span>'. $meta['descrizione_evento'][0].'</span>
                            </div>
                            <div class="col-6 evento">
                                <span>'. $meta['mappa_evento'][0].'</span>
                            </div>
                        </div>
                    </div>
                    
                    
                <footer class="footer-single-gestione-eventi">
                <div>
                    <a href="'.home_url().'/eventi/">
                        <span class="meta-nav">←</span>
                        Torna alla pagina eventi
                    </a>
                </div>
            </footer>';
}

add_action( 'rest_api_init', 'create_api_posts_meta_field' );
 
function create_api_posts_meta_field() {
 
 // register_rest_field ( 'name-of-post-type', 'name-of-field-to-return', array-of-callbacks-and-schema() )
 register_rest_field( 'post', 'post-meta-fields', array(
 'get_callback' => 'get_post_meta_for_api',
 'schema' => null,
 )
 );
}
 
function get_post_meta_for_api( $object ) {
 //get the id of the post object array
 $post_id = $object['id'];
 
 //return the post meta
 return get_post_meta( $post_id );
}


?>