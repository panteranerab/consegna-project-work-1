<?php get_header(); ?>
    <main id="content">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    
       <?php //get_template_part('entry'); ?>      
        <?php if ( ! post_password_required() ) comments_template( '', true ); ?>
        <?php endwhile; endif; ?>

        <!-- creo una variabile $meta che cotiene tutti i post_meta inseriti nel custom post Gestione eventi -->
        <?php $meta = get_post_meta(get_the_id());?>
      
        <!--******* inizio template post gestione_evento ******* -->

        <?php evento_impaginazione($meta) ?>

        <?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>
    </main>
    <?php get_sidebar(); ?>
<?php get_footer(); ?>

