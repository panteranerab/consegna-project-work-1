<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
<header id="header" style="background-image:url(<?php if ( get_header_image() ) { echo esc_url( get_header_image() ); } else { echo esc_url( get_template_directory_uri() ) . '/images/bg.jpg'; } ?>)">
<div id="header-nav">
<div id="site-title">
<?php
$custom_logo_id = get_theme_mod( 'custom_logo' );
$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '<h1>'; }
echo '<a href="' . esc_url( home_url( '/' ) ) . '" title="' . esc_attr( get_bloginfo( 'name' ) ) . '" rel="home">';
if ( has_custom_logo() ) {
echo '<img src="' . esc_url( $logo[0] ) . '" id="logo" />';
} else {
bloginfo( 'name' );
}
echo '</a>';
if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '</h1>'; }
?>
</div>
<nav id="menu">
<div id="search">
<?php get_search_form(); ?>
</div>
<label class="toggle" for="toggle"><span class="menu-icon">&#9776;</span> <?php esc_html_e( 'Menu', 'services' ); ?></label>
<input id="toggle" class="toggle" type="checkbox" />
<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
</div>
</nav>
<div id="site-description"><?php bloginfo( 'description' ); ?></div>
<?php if ( is_active_sidebar( 'header-widget-area' ) ) : ?>
<aside id="header-sidebar" role="complementary">
<div id="hsidebar" class="widget-area">
<ul class="xoxo">

    <button class="widget-container widget_pages header-link">
        <h3 class="widget-title"><a href="/centro-storico/">Il Centro Storico <i class="fas fa-archway"></i></a></h3>
    </button>
    <button class="widget-container widget_pages">
        <h3 class="widget-title"><a href="/spiaggia/">La Spiaggia <i class="fas fa-umbrella-beach"></i></a></h3>
    </button>
    <button class="widget-container widget_pages">
        <h3 class="widget-title"><a href="/entroterra/">L'Entroterra <i class="fas fa-mountain"></i></a></h3>
    </button>
    <button class="widget-container widget_pages">
        <h3 class="widget-title"><a href="/gastronomia/">La Gastronomia <i class="fas fa-utensils"></i></a></h3>
    </button>

</ul>
<div class="clear"></div>
</div>
</aside>
<?php endif; ?>
</header>
<div id="container">
